from pyVoIP.VoIP import VoIPPhone, InvalidStateError


def answer(call):
    try:
        call.answer()
        call.hangup()
    except InvalidStateError:
        pass


if __name__ == '__main__':
    # SIP SERVER IP ADDRESS
    SIP_IP = ""

    # SIP SERVER PORT
    SIP_PORT = 5060

    # SIP USERNAME
    SIP_USERNAME = ""

    # SIP PASSWORD
    SIP_PASSWORD = ""

    # IP ADDRESS OF LOCAL MACHINE
    LOCAL_IP = ""

    phone = VoIPPhone(
        SIP_IP,
        SIP_PORT,
        SIP_USERNAME,
        SIP_PASSWORD,
        callCallback=answer,
        myIP=LOCAL_IP
    )

    phone.start()
    input('Press enter to disable the phone')
    phone.stop()
